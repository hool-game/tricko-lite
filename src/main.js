import "./app.css";
import App from "./App.svelte";

// For Node <-> browser compatability
import "core-js/stable";
import "regenerator-runtime/runtime";
import {TrickoClient} from "tricko-client";
import Tricko from "./tricko.js";

// Set the default login and resource
let defaultLogin = import.meta.env.VITE_XMPP_DOMAIN || "faeko.hool.org";

let resource = import.meta.env.VITE_XMPP_RESOURCE || "faeko-lite";

let client = new TrickoClient({
  service: import.meta.env.VITE_XMPP_WEBSOCKET,
  resource: resource,
});

if (import.meta.env.VITE_XMPP_GAME_SERVICE) {
  client.gameService = import.meta.env.VITE_XMPP_GAME_SERVICE;
}

let tricko = new Tricko({}, client);

const app = new App({
  target: document.getElementById("app"),
  props: {
    client,
    tricko,
    defaultLogin,
  },
});

// Allow for debugging if needed
if (import.meta.env.VITE_DEBUG) {
  window.Tricko = {
    client,
    tricko,
    app,
  };
}

export default app;
