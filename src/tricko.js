// @ts-nocheck
import cards from "@hool/cards";
import TrickoLogic from "@tricko/logic";

function Tricko(data, client) {
  // Inherit behaviour and functions from TrickoLogic
  TrickoLogic.apply(this, {})

  // DONE: need to reset the occupants array based on the game state
  // DONE: need to create the players array based on the occupants who're inside
  // DONE: check whether the state got updated!

  // DONE: in end of the game play we need to set the score to the occupants,
  // and then we need to render it based on whatever in occupants array.
  // DONE: We have to set the occupants' ready status to false once the score is revealed!

  // TODO: need to create the score object - then add the scores

  // Hack: the data property isn't being set so
  // we're setting it again
  this.data = data

  // A client for sending and receiving messages
  // from the server. This is assumed to handle
  // all the messages in XMPP/JSON/whatever format
  // is being used
  this.client = client
  // if the game ends, we need to set this to true
  // Will help in resetting deck and tricks
  this.data.gameEnd = false

  // Handle client events
  this.client.on("start", ({ game, user }) => {
    console.log(`${user} is ready to play at ${game}`)

    // reset the data for the next game, if it exists?!
    if (this.data.players) {
      delete this.data.players
      delete this.data.scores
      this.data.flipping=false
      this.data.gameEnd = true
      this.data.deck = undefined
      this.data.tricks = new cards.Tricks(this.data.deck)

      // clear the session storage!
      sessionStorage.clear();
    }

    this.data.occupants = this.data.occupants.filter(
      (o) => !o.inactive
    )

    // Update the user
    let userIndex = this.data.occupants.findIndex((o) => o.nickname == user)

    // If the user is there, add them in
    if (userIndex >= 0) {
      this.data.occupants[userIndex].ready = true
      return;
    } else {
      console.warn(`Got start message from non-existent user ""${user}"!`)
    }
  });

  this.client.on("roomJoin", (presence) => {
    // Add user to occupant list if needed
    if (!this.data.occupants) {
      this.data.occupants = [];
    }

    if (this.data.players !== undefined) {
      this.data.players = undefined // or delete the players - delete this.data.players
    }

    let userIndex = this.data.occupants.findIndex(
      (o) => o.jid == presence.user.toString()
    );

    // If user is already there, just update
    if (userIndex >= 0) {
      this.data.occupants[userIndex].role = presence.role
      return
    }

    // Otherwise, add
    this.data.occupants.push({
      jid: presence.user.toString(),
      role: presence.role,
      nickname: presence.nickname,
    })
  })

  this.client.on("roomDisconnect", (presence) => {
    // FIXME: only do this for exit, not disconnect!
    // Remove the user from the occupants list

    if (!this.data.occupants) {
      this.data.occupants = []
    }

    if (!this.data.dealers) {
      this.data.dealers = []
    }

    let userIndex = this.data.occupants.findIndex(
      (o) => o.jid == presence.user.toString()
    );

    // If the user is there, remove them
    if (userIndex >= 0) {
      let removedUser = this.data.occupants[userIndex]
      this.data.occupants[userIndex].inactive = true
      this.data.dealers[userIndex].inactive = true

      // change the inactive flag to true, if the user is a player
      this.removePlayer(removedUser.nickname)
    }

    sessionStorage.clear()
  });

  // for removing players if anyone exited in the midgame
  this.removePlayer = (nickname) => {
    // check for the players array
    if (!this.data?.players || !this.data.players.length) return
    if (!this.data?.dealers || !this.data.dealers.length) return

    // finding out which are the players need ot be removed!
    // get their indices based on the nickname
    let playerIndices = this.data.players.reduce((indices, p, index) => {
      if (p.nickname === nickname) {
        indices.push(index);
      }
      return indices;
    }, []);

    playerIndices.forEach((playerIndex) => {
      if (this.data.take && playerIndex === this.data.take.player) {
        this.data.take.enable = false
      }

      // changing the inactive flag to true
      this.data.players[playerIndex].inactive = true;
      // Check if the dealer exists before setting the inactive flag
      if (this.data.dealers[playerIndex]) {
        this.data.dealers[playerIndex].inactive = true;
      }
    });

    // if all the other players are inactive means game over
    let activePlayers = this.data.players.filter((p) => !p.inactive)

    // if only one active player is left means game over
    if (activePlayers.length === 1) this.data.gameOver = true

    return
  };

  this.client.on("roomExit", (presence) => {
    // Remove the user from the occupants list
    if (!this.data.occupants) {
      this.data.occupants = []
    }

    let userIndex = this.data.occupants.findIndex(
      (o) => o.jid == presence.user.toString()
    );

    // If the user is there, remove them
    if (userIndex >= 0) {
      let removedUser = this.data.occupants[userIndex]
      this.data.occupants[userIndex].inactive = true

      // change the inactive flag to true, if the user is a player
      this.removePlayer(removedUser.nickname)
    }
    
    sessionStorage.clear()
  });

  this.dealerChange = () => {
    // Checking if the user is active or not
    let activeDealers = this.data.dealers.filter((p) => !p.inactive)

    const dealerIndex = activeDealers.findIndex((deal) => deal.dealer);

    if (dealerIndex !== -1) {
      activeDealers.forEach((deal, index) => {
        if (index === dealerIndex) {
          deal.dealer = false
        } else if (index === (dealerIndex + 1) % activeDealers.length) {
          deal.dealer = true
        } else {
          deal.dealer = false
        }
      });
    }

    return activeDealers
  }

  // Handle state updates
  this.client.on("state", (state) => {
    // Okay, so there's a lot to unpack here; let's take this
    // slowly.

    console.log("state for dool \n", state)
    // Start with the deck, since that's the most important
    if (state.deck) {
      if (!this.data.deck) {
        this.data.deck = new cards.Deck();
      }
      this.data.gameEnd = false

      this.data.deck.cards = state.deck;
    }

    // @ts-ignore
    let isItInScoreStage = this.getTurn().type == "score" ? true : false

    // idk, I think it is for resetting the players and dealers at end of the game :(
    // ^--- see that's why you should've commented earlier, while writing
    // the code

    // Now comes the major part: the players!
    if (state.players) {
      // FIXME: This logic shouldn't be here, cuz of
      // at score stage -->  the state will be updated
      // so the player array updating should occur somewhere...

      let playingOccupants = this.data.occupants.filter((o) =>
        o.role.slice(0, 6) == "player" && o.ready)
        
      if (!this.data.players) {
        if (this.getTurn().type !== "score" && !this.data.dealers) {
          this.data.dealers = [];

          playingOccupants.forEach((o, i) => {
            this.data.dealers.push({
              nickname: o.nickname,
              inactive: false,
              dealer: i === 0 ? true : false,
            });
          });
        }

        this.data.players = []
        this.data.dupPlayers = []

        // Automatically compute the nicknames (this can be
        // overridden later

        // let playingOccupants = this.data.occupants.filter((o) =>
        //   o.role.slice(0, 6) == "player" && o.ready)
        //     \________/
        //          \
        //  _________\_________
        // /                   \
        // Forward compatibility: if we decide to make player
        // roles something like 'player-0', 'player-1', etc.
        // later, this logic will still work.

        // A rudimentary check: if there are more players in
        // the list than we have track of occupants, it means
        // there's something wrong and we should probably just
        // wait for the backend to tell us what the names are.

        // correct @badri provide a perfect comment
        // in dool we have to wait for backend to send nickname

        playingOccupants.forEach((o, i) => {
          this.data.players.push({
            nickname: o.nickname,
            jid: o.jid,
            team: o.team
          });
          this.data.dupPlayers.push({
            nickname: o.nickname,
            jid: o.jid,
            team: o.team
          });
        });
      }

      // Now we loop over the data that's just come in
      state.players.forEach((player, i) => {
        // If the player is undefined, it means there's
        // no data, so we can just skip it
        if (!player) return;
        
        // Now let's make sure we have a record for the
        // player, and see what's there to extract from it.
        if (!this.data.players[i]) {
          this.data.players[i] = {}
        }

        if (!this.data.dupPlayers[i]) {
          this.data.dupPlayers[i] = {}
        }

        // Get the nickname
        if (player.nickname) {
          this.data.players[i].nickname = player.nickname
          this.data.dupPlayers[i].nickname = player.nickname
        }

        if (player.team) {
          this.data.players[i].team = player.team
          this.data.dupPlayers[i].team = player.team
        }

        // Get the hand
        if (player.hand) {
          this.data.players[i].hand = player.hand;
          this.data.dupPlayers[i].hand = player.hand;
          // Sort the cards!
          if (!this.data.deck) this.data.deck = new cards.Deck()
          this.data.players[i].hand.sort(this.data.deck.compare)
          this.data.dupPlayers[i].hand.sort(this.data.deck.compare)
        }

        // exposed means player left the room
        if (player.exposed) {
          this.data.players[i].exposed = player.exposed
        }   

        // Get the score (this is the running score, so it
        // isn't essential to the game per se, but needs
        // must...)
        if (typeof player.score == "number" && !isNaN(player.score)) {
          this.data.players[i].score = player.score

          // What is this for - We need to add the score to the occupants
          // array - cuz we are deleting the players array for new user..
          this.data.occupants[i].score = player.score
        }


        // That's it for the loop! This comment is just there
        // to remind you what the following close-brackets are
        // all about ----.----.
        //               |    |
      }) // <------------'    |

      // creating a teams structure to access the players
      // who are in same players key.
      this.data.teams = this.data.occupants
        .filter(occupant => occupant.players)
        .map(occupant => occupant.players);

      this.data.players = this.data.players.map((player, index) => ({ ...player }));
      this.data.dupPlayers = this.data.dupPlayers.map((player, index) => ({ ...player }));
    } // <--------------------'

    // Time to process the tricks. We'll assume the entire set
    // of tricks is sent at once; there's no "partial tricks
    // update for that round" concept here!
    if (state.tricks) {
      // Again, we make sure there are tricks
      if (!this.data.tricks) {
        this.data.tricks = new cards.Tricks()
      }

      // set trump here if specified
      let trump = state.tricks.find((el) => el.name === "trump").children[0]
      this.data.tricks.trumpSuit = trump
      // Now we loop through them, but this is a significantly
      // smaller loop than last time.
      this.data.tricks.loadArray(
        state.tricks.map((trick) =>
          trick.map((play) => ({
            player: play.player,
            card: {
              suit: play.suit,
              rank: play.rank,
            },
          }))
        )
      );

      // Process flip state
      if (this.data.flippedTricks) {
        this.data.flippedTricks.forEach((flipped, i) => {
          if (flipped && this.data.tricks[i]) {
            this.data.tricks[i].flipped = true
          }
        });
      }

      // Don't forget to flip the trick object itself!
      if (
        this.data.tricks.length &&
        this.data.tricks[this.data.tricks.length - 1].flipped
      ) {
        this.data.tricks.flipped = true
      }
    }

    /**
     * If it is in a score stage - i.e., after score state sent,
     * we need to change the user ready status to false, otherwise
     * you cannot start the new game. After that delete the players
     * array also - if it is not done, it won't re-render the new user presence
     */

    if (isItInScoreStage) {
      this.data.occupants.forEach((player) => {
        player.ready = false
      })

      // call a function to set the new dealer! 
      this.data.dealers = this.dealerChange()
    }
    // And that's it! We're all unpacked :D
  })

  this.client.on("card", (card) => {
    // Figure out which player played the card
    let player = card.player;

    // If it wasn't specified, deduce from nickname
    if (typeof card.player != "number") {
      card.player = this.data.players.findIndex(
        (p) => p.nickname == card.nickname
      )
    }

    // Play the card
    this.play(
      Number(player),
      {
        suit: card.suit,
        rank: card.rank,
      },
      false)  // <----.
    //  .-------------'
    //  |
    // That "false" means we don't try to remove the
    // card from the hand if we don't have the hand
    // in the first place. (We don't have other players'
    // hands, so we shouldn't try to see if they have
    // a card, remove it, etc. If we *do* have the hand,
    // eg. our own hand, then it will get processed as
    // usual).

    // TODO: A
  });

  // Not much to handleflip: we assume the
  // backend has already handled it!
  this.client.on("flip", (flip) => {
    // Figure out which player took the trick
    let player = flip.player
    // If it wasn't specified, deduce from nickname
    if (typeof flip.player != "number") {
      player = this.data.players.findIndex(
        (p) => p.nickname == flip.nickname
      )
    }

    this.data.flipping=!this.data.flipping;
    // first we have to add the trick, then only 
    // you should flip it. Otherwise it will flip 
    // the current trick!

    // actually we are flipping!
    this.data.tricks.flip()
  })

  // this is for take of flip
  this.client.on("take", (take) => {
    // Figure out which player took the trick
    let player = take.player
    // If it wasn't specified, deduce from nickname
    if (typeof take.player != "number") {
      player = this.data.players.findIndex(
        (p) => p.nickname == take.nickname
      )
    }
    // first we have to add the trick, then only 
    // you should flip it. Otherwise it will flip
    // the current trick!
    this.data.tricks.addTrick()

    // Need to calculate the trickIndex for getting the last most trickIndex
    let trickIndex= this.data.tricks.length-1;

    this.take(player, trickIndex)
  })

  // chat handler for incomming chat
  this.client.on("chat", (chat) => {
    if (!this.data.messages) {
      this.data.messages = []
    }

    this.data.messages.push({
      from: chat.nickname,
      text: chat.text,
    })
  })

  // setting the trump value 
  this.client.on("trump", (trump) => {
    this.data.tricks[this.data.tricks.length - 1].trumpSuit = trump.value;
    this.data.tricks.trumpSuit=trump.value;
    console.log(trump.value,"valuue changed for trump")
  })
  this.client.on("flipAndTrump", (flipAndTrump) => {
    this.data.tricks[this.data.tricks.length - 1].trumpSuit = flipAndTrump.value;
    this.data.tricks.trumpSuit=flipAndTrump.value;
    let player = flipAndTrump.player
    // If it wasn't specified, deduce from nickname
    if (typeof flipAndTrump.player != "number") {
      player = this.data.players.findIndex(
        (p) => p.nickname == flipAndTrump.nickname
      )
    } 
    this.data.flipping=!this.data.flipping;
    this.data.tricks.flip()
  })
}

export default Tricko
